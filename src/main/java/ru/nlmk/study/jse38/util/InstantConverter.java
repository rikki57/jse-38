package ru.nlmk.study.jse38.util;

import org.apache.commons.lang3.StringUtils;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

public abstract class InstantConverter {
    private InstantConverter() {
    }

    public static String fromInstant(Instant instant) {
        return instant == null ? null : instant.toString();
    }

    public static Instant toInstant(String string) {
        return StringUtils.isBlank(string) ? null : Instant.parse(string);
    }

    public static Instant toInstantTruncatedToDays(String string) {
        return StringUtils.isBlank(string) ? null : truncateToDays(Instant.parse(string));
    }

    public static Instant truncateToDays(Instant input) {
        return input != null ? input.truncatedTo(ChronoUnit.DAYS) : null;
    }
}
