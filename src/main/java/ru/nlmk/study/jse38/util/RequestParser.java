package ru.nlmk.study.jse38.util;

import ru.nlmk.study.jse38.enums.Action;

import java.util.Arrays;
import java.util.List;

public abstract class RequestParser {
    private static final String PARAM_SPLITTER = " ";
    private static final String ACTION_SPLITTER = " ";

    public static Action getAction(String input) {
        Action action;
        int pos = input.indexOf(ACTION_SPLITTER);
        if (pos > 0) {
            action = Action.findAction(input.substring(0, pos));
        } else {
            action = Action.findAction(input);
        }
        if (action == null) {
            action = Action.EMPTY;
        }
        System.out.println(action.getAction());
        return action;
    }

    public static String getParams(String input) {
        int pos = input.indexOf(" ");
        return pos > 0 ? input.substring(pos) : "";
    }

    public static List<String> getParamsList(String input) {
        return Arrays.asList(input.trim().split(PARAM_SPLITTER));
    }
}
