package ru.nlmk.study.jse38.repository;

import ru.nlmk.study.jse38.model.AbstractEntity;

import java.util.*;

public class AbstractMapRepository<T extends AbstractEntity> implements Repository<T> {
    private final Map<Long, T> storage = new HashMap<>();

    @Override
    public Optional<T> create(T input) {
        synchronized (storage) {
            Long id = findNewKey();
            input.setId(id);
            storage.put(id, input);
        }
        return Optional.ofNullable(input);
    }

    private Long findNewKey() {
        if (storage.size() > 0) {
            return Collections.max(storage.keySet()) + 1L;
        }
        return 1L;
    }

    @Override
    public Optional<T> update(T input) {
        storage.put(input.getId(), input);
        return Optional.of(input);
    }

    @Override
    public Optional<T> getById(Long id) {
        return Optional.ofNullable(storage.get(id));
    }

    @Override
    public void delete(Long id) {
        storage.remove(id);
    }

    @Override
    public List<T> getAll() {
        return new ArrayList<>(storage.values());
    }
}
