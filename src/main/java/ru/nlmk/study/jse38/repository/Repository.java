package ru.nlmk.study.jse38.repository;

import ru.nlmk.study.jse38.model.AbstractEntity;

import java.util.List;
import java.util.Optional;

public interface Repository<T extends AbstractEntity> {
    Optional<T> create(T input);

    Optional<T> update(T input);

    Optional<T> getById(Long id);

    void delete(Long id);

    List<T> getAll();
}
