package ru.nlmk.study.jse38.repository.impl;

import ru.nlmk.study.jse38.model.User;
import ru.nlmk.study.jse38.repository.AbstractMapRepository;
import ru.nlmk.study.jse38.repository.UserRepository;

public class UserRepositoryImpl extends AbstractMapRepository<User> implements UserRepository {
    private static volatile UserRepositoryImpl instance = null;

    private UserRepositoryImpl() {
    }

    public static UserRepositoryImpl getInstance() {
        if (instance == null) {
            synchronized (UserRepositoryImpl.class) {
                if (instance == null) {
                    instance = new UserRepositoryImpl();
                }
            }
        }
        return instance;
    }
}
