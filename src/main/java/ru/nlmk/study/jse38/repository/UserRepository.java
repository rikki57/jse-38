package ru.nlmk.study.jse38.repository;

import ru.nlmk.study.jse38.model.User;

public interface UserRepository extends Repository<User> {
}
