package ru.nlmk.study.jse38;

import ru.nlmk.study.jse38.controller.impl.UserControllerImpl;
import ru.nlmk.study.jse38.reader.CommandReader;
import ru.nlmk.study.jse38.repository.impl.UserRepositoryImpl;
import ru.nlmk.study.jse38.service.impl.UserServiceImpl;

public class Main {
    public static void main(String[] args) {
        CommandReader commandReader = new CommandReader();
        commandReader.addController(new UserControllerImpl(UserServiceImpl.getInstance(UserRepositoryImpl.getInstance())));
        commandReader.start();
    }
}
