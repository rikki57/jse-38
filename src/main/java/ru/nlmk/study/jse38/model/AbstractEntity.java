package ru.nlmk.study.jse38.model;

import lombok.Getter;
import lombok.Setter;

public abstract class AbstractEntity {
    @Getter
    @Setter
    Long id;
}
