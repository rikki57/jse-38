package ru.nlmk.study.jse38.model;

import lombok.Builder;
import lombok.Data;

import java.time.Instant;

@Data
@Builder
public class User extends AbstractEntity {
    private String name;
    private String surname;
    private int age;
    private Instant startDate;
    private Instant endDate;
    private String city;
}
