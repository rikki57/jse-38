package ru.nlmk.study.jse38.controller;

public interface UserController extends Controller {
    String createUser(String input);

    String updateUser(String input);

    String findByStartDate(String input);
}
