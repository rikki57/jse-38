package ru.nlmk.study.jse38.controller.impl;

import lombok.extern.log4j.Log4j2;
import ru.nlmk.study.jse38.controller.UserController;
import ru.nlmk.study.jse38.enums.Action;
import ru.nlmk.study.jse38.enums.Status;
import ru.nlmk.study.jse38.service.UserService;
import ru.nlmk.study.jse38.util.RequestParser;

import java.util.List;

@Log4j2
public class UserControllerImpl implements UserController {
    private final UserService userService;

    public UserControllerImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String getAction(String input) {
        log.debug("getAction --> {}", input);
        Action action = RequestParser.getAction(input);
        String args = RequestParser.getParams(input);
        switch (action) {
            case CREATE_USER:
                return createUser(args);
        }
        log.debug("getAction <-- {}", action.getAction());
        return action.getAction();
    }


    @Override
    public String createUser(String input) {
        List<String> paramsList = RequestParser.getParamsList(input);
        //TODO: убрать этот колхоз
        if (paramsList.size() == 6) {
            return userService.createUser(
                    paramsList.get(0),
                    paramsList.get(1),
                    paramsList.get(2),
                    paramsList.get(3),
                    paramsList.get(4),
                    paramsList.get(5));
        }
        return Status.PARAMS_MISMATCH.getMessage();
    }

    @Override
    public String updateUser(String input) {
        return null;
    }

    @Override
    public String findByStartDate(String input) {
        return null;
    }
}
