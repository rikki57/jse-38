package ru.nlmk.study.jse38.controller;

public interface Controller {
    String getAction(String input);
}
