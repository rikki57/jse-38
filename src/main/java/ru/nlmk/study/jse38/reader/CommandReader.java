package ru.nlmk.study.jse38.reader;

import ru.nlmk.study.jse38.controller.Controller;
import ru.nlmk.study.jse38.enums.Action;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CommandReader {
    private final List<Controller> controllers = new ArrayList<>();

    public void addController(Controller controller) {
        controllers.add(controller);
    }

    public void start() {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String input = scanner.nextLine();
            if (Action.EXIT.getAction().equals(input)) {
                break;
            }
            notifyControllers(input.trim());
        }
        System.out.println("System exit");
    }

    private void notifyControllers(String input) {
        controllers.stream().map(controller -> controller.getAction(input)).forEach(System.out::println);
    }
}
