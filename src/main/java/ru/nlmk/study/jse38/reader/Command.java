package ru.nlmk.study.jse38.reader;

import lombok.Data;

@Data
public class Command {
    String action;
    String parameters;
}
