package ru.nlmk.study.jse38.service;

public interface UserService {
    String createUser(String name, String surname, String startDate, String endDate, String city, String age);
}
