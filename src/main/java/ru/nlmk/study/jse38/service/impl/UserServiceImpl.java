package ru.nlmk.study.jse38.service.impl;

import ru.nlmk.study.jse38.enums.Status;
import ru.nlmk.study.jse38.model.User;
import ru.nlmk.study.jse38.repository.UserRepository;
import ru.nlmk.study.jse38.service.UserService;
import ru.nlmk.study.jse38.util.InstantConverter;

public class UserServiceImpl implements UserService {
    private static volatile UserServiceImpl instance = null;
    private final UserRepository userRepository;

    private UserServiceImpl() {
        userRepository = null;
    }

    private UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public static UserServiceImpl getInstance(UserRepository userRepository) {
        if (instance == null) {
            synchronized (UserServiceImpl.class) {
                if (instance == null) {
                    instance = new UserServiceImpl(userRepository);
                }
            }
        }
        return instance;
    }

    @Override
    public String createUser(String name, String surname, String startDate, String endDate, String city, String age) {
        User user = User.builder()
                .name(name)
                .city(city)
                .age(Integer.valueOf(age))
                .startDate(InstantConverter.toInstant(startDate))
                .endDate(InstantConverter.toInstant(startDate))
                .build();
        if (userRepository.create(user).isPresent()) {
            return Status.OK.getMessage();
        }
        return Status.DB_ERROR.getMessage();
    }
}
