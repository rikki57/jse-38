package ru.nlmk.study.jse38.service.impl;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import ru.nlmk.study.jse38.enums.Status;
import ru.nlmk.study.jse38.model.User;
import ru.nlmk.study.jse38.repository.UserRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class UserServiceImplTest {
    private UserRepository userRepository;

    private UserServiceImpl userService;


    @BeforeEach
    void setUp() {
        userRepository = Mockito.mock(UserRepository.class);
        userService = UserServiceImpl.getInstance(userRepository);
    }

    @Test
    void createUser() {
        when(userRepository.create(any())).thenReturn(Optional.of(User.builder().build()));
        String result = userService.createUser("name", "name2", "2020-08-01T12:00:00.000Z",
                "2020-08-01T12:00:00.000Z", "Moscow", "20");
        assertEquals(Status.OK.getMessage(), result);
    }
}